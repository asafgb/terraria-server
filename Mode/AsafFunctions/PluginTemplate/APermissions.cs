﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsafPlugin
{
    public static class APermissions
    {
        //#region Superadmin Commands
        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string AsafCommands = "asaf.perm";

        [Description("User can register account in game.")]
        public static readonly string givePlayerBuff = "user.playerbuff";

        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string userPasswords = "user.pass";

        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string KillingNearMobs = "vip.killNearMobs";

        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string KillingAllMobs = "admin.killNearMobs";

        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string TestCommands = "user.testcomm";

        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string OneTeam = "user.oneteam";

        [Description("Validate If use Asaf Costume Commands")]
        public static readonly string GetAllData = "user.data";


        // #endregion
    }
}
