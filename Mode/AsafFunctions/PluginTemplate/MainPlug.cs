﻿using AsafPlugin;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Terraria;
using Terraria.Localization;
using TerrariaApi.Server;
using TShockAPI;

namespace PluginTemplate
{
    /// <summary>
    /// The main plugin class should always be decorated with an ApiVersion attribute. The current API Version is 1.25
    /// </summary>
    [ApiVersion(2, 1)]
    public class MainPlug : TerrariaPlugin
    {
        /// <summary>
        /// The name of the plugin.
        /// </summary>
        public override string Name => "Asaf Plugin";

        /// <summary>
        /// The version of the plugin in its current state.
        /// </summary>
        public override Version Version => new Version(1, 0, 0);

        /// <summary>
        /// The author(s) of the plugin.
        /// </summary>
        public override string Author => "Asafgb";

        /// <summary>
        /// A short, one-line, description of the plugin's purpose.
        /// </summary>
        public override string Description => "A simple, senseless, plugin used to demonstrate the code structure of a TShock and TS-API plugin.";

        private Timer update;
        private Dictionary<string, List<string>> GroupPermssions = new Dictionary<string, List<string>>();

        /// <summary>
        /// The plugin's constructor
        /// Set your plugin's order (optional) and any other constructor logic here
        /// </summary>
        public MainPlug(Main game) : base(game)
        {

        }

        

        /// <summary>
        /// Performs plugin initialization logic.
        /// Add your hooks, config file read/writes, etc here
        /// </summary>
        public override void Initialize()
        {
            #region Commands
            Commands.ChatCommands.Add(new Command(Permissions.user, InitCostumeComments, "initcoms"));
            Commands.ChatCommands.Add(new Command(APermissions.userPasswords, UserPass, "resetpass"));
            Commands.ChatCommands.Add(new Command(APermissions.givePlayerBuff, GivePlayerBuff, "buffer"));
            Commands.ChatCommands.Add(new Command(APermissions.KillingAllMobs, KillingAllMobs,"killallmobs"));
            Commands.ChatCommands.Add(new Command(APermissions.KillingNearMobs, KillingNearMobs, "killnearmobs","killnearme"));
            Commands.ChatCommands.Add(new Command(APermissions.KillingNearMobs, KillingNearMobs, "killnearmobs", "killnearme"));
            Commands.ChatCommands.Add(new Command(APermissions.TestCommands, TestCommands, "test"));
            Commands.ChatCommands.Add(new Command(APermissions.OneTeam, OneTeam, "oneteam"));
            Commands.ChatCommands.Add(new Command(APermissions.GetAllData, GetAllData, "getdata"));

            #endregion


            update = new Timer { Interval = 1000, AutoReset = true, Enabled = true };
            update.Elapsed += OnElapsed;



            ServerApi.Hooks.NetGreetPlayer.Register(this, OnGreet);


            #region GroupsPermissionsCheck

                GroupPermssions.Add("owner", new List<string>()
                {
                      APermissions.AsafCommands,
                      APermissions.givePlayerBuff,
                    APermissions.userPasswords,
                    APermissions.OneTeam,
                    APermissions.GetAllData,
                    "pb.check",
                    "pb.give",
                    "pb.clear",
                    "pb.region",
                    "pb.global",
                    "pb.reload",
                    "ic.edit",
                    "customitem",
                    "customitem.give",
                });

                GroupPermssions.Add("admin", new List<string>()
                {
                       APermissions.KillingAllMobs
                });

                GroupPermssions.Add("vip", new List<string>()
                {
                      APermissions.KillingNearMobs
                });

                GroupPermssions.Add("guest", new List<string>()
                {
                      APermissions.TestCommands,
                    "tshock.ignore.*",
                    "tshock.tp.rod",
                    "tshock.world.editspawn",
                    "tshock.npc.summonboss",
                    "tshock.world.movenpc",
                    "tshock.npc.hurttown",
                    "ic.protect",
                    "ic.use",
                    "ic.info",
                    "ic.search",
                    "tshock.npc.startdd2",
                });
                #endregion

            }

        private void OnElapsed(object sender, ElapsedEventArgs e)
        {
            for (int i = 0; i < TShock.Players.Length; i++)
                if(TShock.Players[i].Team!=2)
                TShock.Players[i].SetTeam(2);
            Item A = new Item();
            
        }
        private void OneTeam(CommandArgs args)
        {
            update.Enabled = !update.Enabled;
            args.Player.SendMessage("timer status: " + update.Enabled,Color.AliceBlue);
        }

        private void GetAllData(CommandArgs args)
        {
            string subCmd = args.Parameters.Count == 0 ? "help" : args.Parameters[0].ToLower();
            string UserName = args.Parameters[0];
            int buffid = 0, time = 0;
            if (!TSPlayer.FindByNameOrID(UserName).Any())
            {
                args.Player.SendMessage("No such as that UserName", Color.Yellow);
                return;
            }
            TSPlayer player = TSPlayer.FindByNameOrID(UserName).First();
            Console.WriteLine($"UUID:{player.UUID}\r\n IP: {player.IP}\r\n Name: {player.Name} ");
        }

        private void TestCommands(CommandArgs args)
        {
            
            Console.WriteLine(args.Player.Team);
            Console.WriteLine(args.Player.ItemInHand.owner);
        }

        private void KillingNearMobs(CommandArgs args)
        {
            /* // int range = args.Parameters.Count == 0 ? 1000 : int.Parse(args.Parameters[0]);
              Console.WriteLine("Mine:"+ args.TPlayer.position);
              List<NPC> npcs = Main.npc.Where(npc=>!(npc.friendly) && npc.Distance(args.TPlayer.position)< 600).ToList();
              foreach (NPC npc in npcs)
              {
                  npc.StrikeNPC(int.MaxValue - 1, 10, 1);
                  NetMessage.SendData((int)PacketTypes.NpcStrike, -1, -1, NetworkText.Empty, 1, int.MaxValue, 10, 1);
                  //Console.WriteLine("EnemyInRange:" + npc.position+ " name:" + npc.TypeName);
                  //Console.WriteLine(npc.GivenName + ":" + npc.friendly);
              }
              */
            NPC npc;
            for(int i=0; i< Main.npc.Length;i++)
            {
                npc = Main.npc[i];
                if(!(npc.friendly) && npc.Distance(args.TPlayer.position) < 650)
                {
                    npc.StrikeNPC(int.MaxValue - 1000, 10, 1,true);
                    NetMessage.SendData((int)PacketTypes.NpcStrike, -1, -1, NetworkText.Empty, i, int.MaxValue- 1000, 10, 1);
                }
            }
        }

        private void KillingAllMobs(CommandArgs args)
        {
            NPC npc;
            for (int i = 0; i < Main.npc.Length; i++)
            {
                npc = Main.npc[i];
                if (!(npc.friendly))
                {
                    npc.StrikeNPC(int.MaxValue - 1000, 10, 1, true);
                    NetMessage.SendData((int)PacketTypes.NpcStrike, -1, -1, NetworkText.Empty, i, int.MaxValue - 1000, 10, 1);
                }
            }
        }

        private void OnGreet(GreetPlayerEventArgs args)
        {
            //Console.WriteLine(TShock.Players[args.Who].PlayerData.maxHealth);
            //TShock.Players[args.Who].TPlayer.team = 4;
            //TShock.Players[args.Who].SetTeam(4);

        }

        private void InitCostumeComments(CommandArgs args)
        {

            foreach (string key in GroupPermssions.Keys)
            {
                foreach (string permisson in GroupPermssions[key])
                {
                    if (!TShock.Groups.GetGroupByName(key).HasPermission(permisson))
                    {
                        TShock.Groups.AddPermissions(key, new List<string>()
                        {
                            permisson
                        });
                        args.Player.SendMessage($"Group: {key} Permission: {permisson} Added", Color.Aquamarine);
                    }
                }
            }
        }

        private void GivePlayerBuff(CommandArgs args)
        {
            string subCmd = args.Parameters.Count == 0 ? "help" : args.Parameters[0].ToLower();
            switch (subCmd)
            {
                case "help":
                    #region Help
                    {
                        args.Player.SendMessage("/buffer <username> <buffid> <TimeInSeconds>", Color.Yellow);
                        return;
                    }
                    #endregion
                    
                default:
                    {
                        try
                        {
                            string UserName = args.Parameters[0];
                            int buffid = 0, time = 0;
                            if (!TSPlayer.FindByNameOrID(UserName).Any())
                            {
                                args.Player.SendMessage("No such as that UserName", Color.Yellow);
                                return;
                            }
                            TSPlayer player = TSPlayer.FindByNameOrID(UserName).First();

                            if (!int.TryParse(args.Parameters[1], out buffid))
                            {
                                args.Player.SendMessage("No such as that buffid", Color.Yellow);
                            }

                            if (!int.TryParse(args.Parameters[2], out time))
                            {
                                args.Player.SendMessage("Put number for time > 0", Color.Yellow);
                            }



                            player.SetBuff(buffid, time * 1000, true);
                            return;
                        }
                        catch (Exception x)
                        {
                            args.Player.SendErrorMessage(x.Message);
                            return;
                        }
                    }
            }
            

        }


        public void UserPass(CommandArgs args)
        {
            string subCmd = args.Parameters.Count == 0 ? "help" : args.Parameters[0].ToLower();
            switch (subCmd)
            {
                case "help":
                    #region Help
                    {
                        args.Player.SendMessage("/resetpass <username> ", Color.Yellow);
                        return;
                    }
                    #endregion
            }
            try
            {
                string UserName = args.Parameters[0];
                string newPassword = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 10);
                TSPlayer userDest = TSPlayer.FindByNameOrID(UserName).First();
                var Account = TShock.UserAccounts.GetUserAccounts().Where(u => u.UUID == userDest.UUID && u.Name == userDest.Name).First();
                

                TShock.UserAccounts.SetUserAccountPassword(Account, newPassword);
                userDest.SendMessage($"the password for user {UserName} is {newPassword}", 100, 100, 100);
                args.Player.SendMessage($"the password for user {UserName} is {newPassword}", 100, 100, 100);
            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }
        }

        //public void giveBuff(CommandArgs args)
        //{
        //    try
        //    {
        //        string UserName = args.Parameters[0];
        //        string newPassword = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 10);
        //        TSPlayer userDest = TSPlayer.FindByNameOrID(UserName).First();
        //        var Account = TShock.UserAccounts.GetUserAccounts().Where(u => u.UUID == userDest.UUID && u.Name == userDest.Name).First();

        //        TShock.UserAccounts.SetUserAccountPassword(Account, newPassword);
        //        e.Player.SendMessage($"the password for user {UserName} is {newPassword}", 100, 100, 100);
        //    }
        //    catch (Exception x)
        //    {
        //        Console.WriteLine(x.Message);
        //    }
        //}

        /// <summary>
        /// Performs plugin cleanup logic
        /// Remove your hooks and perform general cleanup here
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //unhook
                //dispose child objects
                //set large objects to null
            }
            base.Dispose(disposing);
        }
    }
}


