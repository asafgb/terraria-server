//'Import' necessary packages
using System;
using System.Timers;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TShockAPI;
using TShockAPI.Hooks;
using TShockAPI.DB;
using Terraria;
using Terraria.Localization;
using TerrariaApi.Server;
using Microsoft.Xna.Framework;

namespace SpawnCooldown
{
	//Version of TShock API
	[ApiVersion(2,1)]
	
	public class SpawnCooldown : TerrariaPlugin
	{
		//Get version of SpawnCooldown
		public override Version Version
		{
			get { return new Version(1, 0); }
		}
		
		//Name the SpawnCooldown
		public override string Name
		{
			get { return "Spawn Cooldown"; }
		}
		
		//Who created the SpawnCooldown
		public override string Author
		{
			get { return "Mariothedog"; }
		}
		
		//Describe the SpawnCooldown (not necessary)
		public override string Description
		{
			get { return "Adds a 5 second cooldown to the /spawn command while also giving the user the slow debuff."; }
		}
		
		//Will be run first by TShock
		//-x will be run after TShock API loads
		//+x will be run before TShock API loads
        public SpawnCooldown(Main game)
			: base(game)
		{
            Order = +4;
		}

        //Main code area
        public override void Initialize()
		{
            Commands.ChatCommands.Remove(Commands.TShockCommands.Where(c => c.Name == "spawn").First());
            Commands.ChatCommands.Remove(Commands.TShockCommands.Where(c => c.Name == "home").First());
            Commands.ChatCommands.Add(new Command("SpawnCooldown", WarpCooldown, "middle", "mid", "s", "spawncooldown", "sc", "spawn", "home"));
        }

        Timer timer = new Timer(5000); // Timer for 5 seconds
        Timer timerMessage = new Timer(1000); // Timer for 1 second
        int cooldown = 5;

        void WarpCooldown(CommandArgs args)
        {
            if (args.Player.TPlayer.statLife < args.Player.TPlayer.statLifeMax2) // If the player doesn't have full health
            {
                if (timer.Enabled)
                {
                    args.Player.SendMessage("Please don't spam that command!", Color.Orange);
                }
                else
                {
                    cooldown = 5;
                    timer = new Timer(5000);
                    timerMessage = new Timer(1000);
                    timerMessage.Elapsed += (sender, e) => Message(sender, e, args);
                    timer.Elapsed += (sender, e) => Teleport(sender, e, args);
                    timerMessage.Start();
                    timer.Start();

                    args.Player.SetBuff(32, 300); // Slow debuff for 5 seconds
                    args.Player.SendMessage("There are 5 seconds left until you are teleported!", Color.Orange);
                }
            }
            else
            {
                Teleport(args);
            }
        }

        void Message(object sender, ElapsedEventArgs e, CommandArgs args)
        {
            cooldown -= 1;
            if (cooldown == 1)
            {
                args.Player.SendMessage("There is " + cooldown + " second left until you are teleported!", Color.Orange);
            }
            else
            {
                args.Player.SendMessage("There are " + cooldown + " seconds left until you are teleported!", Color.Orange);
            }
        }

        void Teleport(object sender, ElapsedEventArgs e, CommandArgs args) // For the timer
        {
            timerMessage.Stop();
            timer.Stop();
            if (args.Player.Teleport(Main.spawnTileX * 16, (Main.spawnTileY * 16) - 48))
            {
                args.Player.SendSuccessMessage("Teleported to the map's spawnpoint.");
            }
        }

        void Teleport(CommandArgs args) // Not for the timer
        {
            if (args.Player.Teleport(Main.spawnTileX * 16, (Main.spawnTileY * 16) - 48))
            {
                args.Player.SendSuccessMessage("Teleported to the map's spawnpoint.");
            }
        }
    }
}